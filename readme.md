
## Brandon Lyon

[https://twitter.com/brandon_m_lyon](https://twitter.com/brandon_m_lyon)

[https://about.brandonmlyon.com/](https://about.brandonmlyon.com/)

## Before you begin:

Please read this entire readme before visiting or running the project. Context and documentation are important in life.

## This project is hosted at:

[https://pplanhw.firebaseapp.com](https://pplanhw.firebaseapp.com)

and was based on

[https://github.com/mark-barbir/productplan-candidate-homework](https://github.com/mark-barbir/productplan-candidate-homework)

## Preamble:

I've only tested this in Chrome & Firefox. I have decades of experience supporting other browsers. I'm fully aware that CSS variables do not work in older browsers without a polyfill and that I could have used a preprocessor.

The React framework I chose is [Next.JS](https://nextjs.org/). Think of Next.JS as create-react-app with a few extras built in like server-side rendering and routing.

## In order to get the project running (assuming you have an appropriate version of node, else start with NVM install stable)...

1. npm install -g serve
2. git clone [https://bitbucket.org/brandonmlyon/blyon-pplanhw.git](https://bitbucket.org/brandonmlyon/blyon-pplanhw.git)
3. cd blyon-pplanhw
4. npm install
5. cd out
6. npm run build && npm run export && serve -p 8080

Why cd out? Next.JS precompiles the app and that's the output folder. Since you're serving those files you'll either want to cd into the out folder or specify a path for serve. You could also use Next's built in build / start / deploy, but this is easier.

I chose Firebase and Bitbucket to host this demo because they're free and easy and where I host most of my stuff. I've used AWS plenty but its overly convoluted for most small projects.

## Tests

My personal favorite testing frameowrk is [cypress.io](https://www.cypress.io) but I have used others like selenium.

If you just want to read the test files, they're in the /cypress/integration directory.

If you want to run the tests, navigate to the project directory, start the web server using the steps above, and in another terminal run

npx cypress open

After that, double click on a test to run it. Click or hover on an item in the left pane to view more details.

## Notes:

For this demo I'm assuming the user has already been authenticated and is allowed to read/write/modify all data present.

The JS for the main drag-n-drop functionality is in one monolithic file because it was written by one person, won't be reused, is only applied to one page, and isn't overly huge. I normally would not code a monolithic file like this. I would break the js into multiple different react components.

There are different approaches for how walkthroughs might work, each with pros & cons. To make it easy I just used hide+show rather than async etc. I ommitted the close button on the walkthrough because from a UX perspective it is both redundant and unclear what the expected functionality would be. The "Got It" button should suffice.

This is a frontend test so for demo simplicity didn't end up implementing data storage. I know that data should be checked on the server and the client sides. Cookies are one option. Other storage optons include: states (React, MobX, ClearX, etc), webstorage, directly into db, abstracted via GraphQL, accessed via API, pub/sub frameworks like Meteor, SQS, placed into a router function for pathing later, flatfile, etc.

CSS code depends on the environment which it's written for. I'm quickest with global namespace vanilla CSS from scratch, so that's what I used to expedite building the demo. I've also worked with CSS-in-JS, namespaced components, shadow DOMs, SASS / LESS, variables & calc, frameworks like bootstrap, and native app styling technologies. I'm well versed in more advanced CSS tech like relative units vs absolute units, mediaqueries / container queries, grids / flexbox, and attribute query selectors. I'm conscious of human physiology when it comes to things like accessibility contrast ratios, fat fingers on tiny touchscreens, screen density, and the impact of distance on legibility.

## Mockup Details:

There were multiple different color values used for black in the mockup. I just picked one value to use for consistency. The same goes for border colors and some other items.

The font family in the mockup screenshots is different than the Gilroy fonts referenced by the sketch file. I'm pretty sure the mockup screenshots are using FreeSans or something like it. I stuck with Gilroy since that's the font which was provided for download.

The second word of Product roadmap uses a lowercase R but in Candidate Roadmap the R is capitalized. This also happens for the navigation tabs on the right. For consistency I used title case since that's what the mockup uses in some other spots.

The Lane titles and lane items have different border radii in the mockup but I made them the same.

The sentence "drag and drop a line to get started" was missing a period at the end so I added one. There were some typos in the walkthrough popovers as well.

The popovers had inconsistent dimensions. The third was 10px wider. Some of the margins were different. I tried to make the dimensions consistent.

## PS:

Extras I left out due to time:

* Dynamically coded lane quarter labels, relative to timestamps, with coordinates for drag/drop locations.
* Different cursors and styles shown while dragging an item.
* Protecting types of droppables and targets from dragging depending on which step you're on.
* Validating/invalidating drops & related error messages.
* User state / data storage.
* More tests.
* More animations.
* Some functionality commented in /js/index.js and /js/pseudocode.js.
