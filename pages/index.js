import Layout from '../components/LayoutMain.js'
import Roadmap from '../components/Roadmap.js'
import Walkthrough from '../components/Walkthrough.js'
import Link from 'next/link'

export default () => (
  <Layout>
    <main>
      <Roadmap />
      <Walkthrough />
      <script src="../static/js/index.js"></script>
    </main>
  </Layout>
)
