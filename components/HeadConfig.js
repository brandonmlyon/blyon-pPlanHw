import Head from 'next/head'
export default () => (
  <div>
    <Head>
      <meta name="robots" content="noindex, nofollow" />
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
      <meta charSet="utf-8" />
      <title>ProductPlan Product Roadmap</title>
    </Head>
  </div>
)
