import '../static/css/components/Walkthrough.css'

const Layout = (props) => (
   <div id="walkthrough">
      <div class="walkthroughWrapper walkthroughStep0 hidden">
         <div class="walkthroughInner">
            <img src="/static/images/screenshot-addLane.jpg" />
            <h4>We'll start with a lane.</h4>
            <p>Lanes represent high level categories, such as teams, product lines, or strategic initiatives. Add a color and description to your lane to communicate valuable details to stakeholders.</p>
            <p>Drag and drop a lane to get started.</p>
            <div class="buttons">
               <div class="button button-primary">Got it</div>
            </div>
         </div>
         <div class="walkthroughArrow"></div>
      </div>
      <div class="walkthroughWrapper walkthroughStep1 hidden">
         <div class="walkthroughInner">
            <img src="/static/images/screenshot-addBars.jpg" />
            <h4>Awesome! Now let's add a few bars.</h4>
            <p>Bars are your specific initiative. Use them to represent your epics, projects, or tasks, and provide an at a glance view of priority, relationships, and progress.</p>
            <p>Drag and drop a bar to get started.</p>
            <div class="buttons">
               <div class="button button-primary">Got it</div>
            </div>
         </div>
         <div class="walkthroughArrow"></div>
      </div>
      <div class="walkthroughWrapper walkthroughStep2 hidden">
         <div class="walkthroughInner">
            <h4>Alright let's set up a couple more.</h4>
            <p>Once they're added, you can share out your roadmap with your team.</p>
            <div class="buttons">
               <div class="button button-primary">Got it</div>
            </div>
         </div>
         <div class="walkthroughArrow"></div>
      </div>
   </div>
)

export default Layout
