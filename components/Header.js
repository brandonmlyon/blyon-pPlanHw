import Link from 'next/link'
import Css from '../static/css/components/Header.css'

const Header = () => (
  <div class="headerWrapper">
    <header>
      <nav class="mainNav">
        <a class="headerLogo" href="/" title="ProductPlan"></a>
        <a href="#" title="Candidate Roadmap"><span>Candidate Roadmap</span></a>
        <a class="headerSearchButton" href="#" title="Search"></a>
      </nav>
      <nav class="secondaryNav">
        <h2 class="navCurrent">Product Roadmap</h2>
        <div class="navTabs">
          <a class="selected" href="#">Roadmap</a>
          <a href="#">Planning Board</a>
          <a href="#">Parking Lot</a>
        </div>
      </nav>
    </header>
  </div>
)

export default Header
