describe('CompleteWalkthrough', function() {
   it('Complete a full run of the walkthrough' , function() {
      cy.visit('http://localhost:8080')
      // normally I wouldn't hardcode this but I don't feel like troubleshooting my environment variables at the moment.

      cy.get('.walkthroughStep0')
         .should('be.visible')
      cy.get('.walkthroughStep0 .button-primary')
         .click()
      cy.get('.walkthroughWrapper')
         .should('not.be.visible')

      cy.get('.roadmapDroppableLane')
         .trigger('mousedown', {which:1})
      cy.get('.laneDropTarget')
         .trigger('mousemove')
         .trigger('drop', {which:1})

      cy.get('.walkthroughStep1')
         .should('be.visible')
      cy.get('.walkthroughStep1 .button-primary')
         .click()

      cy.get('.roadmapDroppableLane[draggable=false]')
         .should('exist')

      cy.get('.roadmapDroppableBar')
         .trigger('mousedown', {which:1})
      cy.get('.barTarget0')
         .trigger('mousemove')
         .trigger('drop', {which:1})

      cy.get('.walkthroughStep2')
         .should('be.visible')
      cy.get('.walkthroughStep2 .button-primary')
         .click()

      cy.get('.roadmapDroppableBar')
         .trigger('mousedown', {which:1})
      cy.get('.barTarget1')
         .trigger('mousemove')
         .trigger('drop', {which:1})
      cy.get('.roadmapDroppableBar').first()
         .trigger('mousedown', {which:1})
      cy.get('.barTarget2')
         .trigger('mousemove')
         .trigger('drop', {which:1})

   })
})
