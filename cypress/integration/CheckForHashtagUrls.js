describe('CheckForHashtagUrls', function() {
   it('Tests URLs so production doesn\'t  have broken urls #' , function() {
      cy.visit('http://localhost:8080')

      cy.log('NOTE! THIS TEST IS SUPPOSED TO FAIL')
      cy.log('BECAUSE IN THIS DEMO THERE ARE')
      cy.log('NON-FUNCTIONING NAVIGATION ITEMS')
      cy.log('SUCH AS PLANNING BOARD, PARKING LOT.')
      cy.log('SEARCH, ETC.')

      cy.get('a')
         .nextAll().should('not.have.attr', 'href', '#')
   })
})
