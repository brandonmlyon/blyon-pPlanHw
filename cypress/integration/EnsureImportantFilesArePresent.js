describe('EnsureImportantFilesArePresent', function() {
   it('Ensure important files are present on server' , function() {
      cy.visit('http://localhost:8080')

      cy.readFile('/static/images/logo.svg')
      cy.readFile('/static/images/screenshot-addLane.jpg')
      cy.readFile('/static/images/screenshot-addBars.jpg')

   })
})
