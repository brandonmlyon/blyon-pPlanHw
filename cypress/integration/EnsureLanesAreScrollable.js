describe('EnsureLanesAreScrollable', function() {
   it('Ensures lanes are scrollable' , function() {
      cy.visit('http://localhost:8080')

      cy.get('.roadmapLanes')
         .should('have.css', 'overflow-y')

      cy.get('.roadmapLanes')
         .should('have.css', 'overflow-y')

   })
})
